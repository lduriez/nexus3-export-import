#!/usr/bin/env groovy
import com.javapda.nexus3.nei.Nexus3ExportImport
import com.javapda.nexus3.nei.domain.*
import com.javapda.nexus3.util.time.*

def startTime=new Date()
println("start:    ${startTime}")
println("args: ${args}")
Nexus3ExportImport.main(args)

def endTime=new Date()
def delta=(endTime.getTime()-startTime.getTime())/1000
println("end:      ${endTime}")
println("seconds:  ${Math.round(delta)} seconds")
println("duration: ${new DurationUtil().duration(startTime,endTime)}")

