#!/bin/bash
############################################################
# PURPOSE: template for new scripts
# hold some usage entries
############################################################
. $(dirname "${0}")/lib.sh

_usage() {
    echo "
  ${YELLOW}USAGE${_normal}
    $_progname [options] 

  ${YELLOW}OPTIONS${_normal}
    -h, --help                               show this help
    --nexus-url <nexus_url>                  specify base url [default: ${nexus3Url}]
    --verbose                                more output

  ${YELLOW}EXAMPLES${_normal}
    $_progname --help
    $_progname 
    $_progname --nexus-url http://localhost:8089
    $_progname --nexus-url https://your.domain.com/nexus

    "
}

_showParams() {
    echo "
       *** parameters ***
       nexus3Url:          ${nexus3Url}
       verbose:            ${verbose}
    "
}



nexus3Url="http://localhost:8089"

while [ $# -gt 0 ]
do
    case "$1" in
        -h|--help)
            _usage
            exit 0
            ;;

        --nexus-url)
            shift
            nexus3Url="${1}"
            ;;

        --verbose)
            verbose=true
            ;;

        *)
            abort "Do not know how to process option '$1'"
            ;;

    esac
    shift
done
[[ -z "${verbose+x}" ]] || _showParams
_nexusVersion "${nexus3Url}"