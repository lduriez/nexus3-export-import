
# expanding a virtual machine job

* when copying the sonatype-work directory (~147GB) I realized that I only had about 16GB remaining
so if I continued I would have filled the hard-drive to the max - bad idea.
So I went to the settings for the Centos VM and ADD NEW DEVICE of 200GB

## edit vm

![image of adding new hard-drive to vm](../images/vm-addnewdevice-hard-drive-200gb.png "Added new device of Hard-Drive of 200GB"  )
## from vm console
```
df -h
dmesg|tail -n 50  # to identify /dev/sdb
pvcreate /dev/sdb
lvextend -L+199G centos/root
xfs_growfs /
df -h
```
