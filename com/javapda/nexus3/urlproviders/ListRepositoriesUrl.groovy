package com.javapda.nexus3.urlproviders

import groovy.transform.ToString

@ToString(includeNames=true, includeFields=true, includePackage=false)
class ListRepositoriesUrl extends Nexus3UrlProvider {

    ListRepositoriesUrl(String nexus3Url) {
        super(nexus3Url)
        path="service/rest/v1/repositories"
    }

    static main(args) {
        println(new ListRepositoriesUrl("http://fake/nexus"))
        println(new ListRepositoriesUrl("http://fake/nexus").build())
    }
}