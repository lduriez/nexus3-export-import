package com.javapda.nexus3.urlproviders
import com.javapda.nexus3.nei.domain.*
import groovy.transform.ToString

@ToString(includeNames=true, includeFields=true, includePackage=false)
class SearchAssetsUrl extends Nexus3UrlProvider {

    SearchAssetsUrl(String nexus3Url) {
        super(nexus3Url)
        path="service/rest/v1/search/assets"
    }

    String get(Nexus3SearchAssetsConfig nexus3SearchAssetsConfig) {
        String queryString = nexus3SearchAssetsConfig.asQueryString()
        String url = "${nexus3Url}/${path}"
        if (queryString.length() > 0) {
            url="${url}?${queryString}"
        }
        url

    }

    static main(args) {
        println(new ListRepositoriesUrl("http://fake/nexus"))
        println(new ListRepositoriesUrl("http://fake/nexus").build())
    }
}