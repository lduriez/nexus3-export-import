package com.javapda.nexus3.urlproviders
import com.javapda.nexus3.util.Nexus3UrlBuilder

import groovy.transform.ToString

@ToString(includeNames=true, includeFields=true, includePackage=false)
abstract class Nexus3UrlProvider extends Nexus3UrlBuilder {
    Nexus3UrlProvider(nexus3Url) {
        this.nexus3Url = nexus3Url
    }


}