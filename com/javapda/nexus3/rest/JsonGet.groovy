package com.javapda.nexus3.rest
import groovy.transform.ToString
import groovy.json.JsonSlurper
import com.javapda.nexus3.nei.domain.*
import com.javapda.nexus3.rest.*
import com.javapda.nexus3.urlproviders.*
import com.javapda.nexus3.nei.action.*
import com.javapda.nexus3.nei.exceptions.*

@ToString(includeNames=true, includeFields=true, includePackage=false)
class JsonGet {
    Credentials credentials
    String url

    def get() {
        def connection = new URL( url )
                .openConnection() as HttpURLConnection
        
        if (!credentials.empty) {
            String basicAuth = 
                "Basic ${new String(Base64.encoder.encode(credentials.colonSeparatedText.getBytes()))}";
            connection.setRequestProperty ("Authorization", basicAuth);
        } else {
            println("no credentials")
        }

        // set some headers
        connection.setRequestProperty( 'User-Agent', 'javapda-groovy-2.4.4' )
        connection.setRequestProperty( 'Accept', 'application/json' )
        // get the response code - automatically sends the request
        String text = connection.inputStream.text.trim()
        if (isValidJson(text)) return text
        throw new BadJsonException("invalid json returned from '${url}'")
    }

    boolean isValidJson(String text) {
        try {
            new JsonSlurper().parseText(text)
            return true
        } catch(exc) {
        }
            return false
    }

    static main(args) {
        Nexus3ExportImportPreferences preferences = Nexus3ExportImportPreferences.loadLocal()
        assert preferences
        Nexus3Installation installation = preferences.getInstallation("localhost-fake")
        assert installation
        String json =
            new JsonGet(
                credentials:installation.credentials, 
                url: new ListRepositoriesUrl(installation.nexus3Url)
                .build()).get()
        List<Nexus3Repo> nexus3Repositories = new JsonSlurper().parseText(json) as List<Nexus3Repo>
        nexus3Repositories.sort{l,r->l.name<=>r.name}.each{ it -> println(it)}
        nexus3Repositories.collect{it->it.name}.sort().each{ it -> println(it)}
        println("${nexus3Repositories.size()} repositories:" )
    }


}