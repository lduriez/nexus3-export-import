package com.javapda.nexus3.rest

class QueryStringBuilder {
    Map params

    String get() {
        params.collect { k,v -> "$k=$v" }.join('&')
    }

}