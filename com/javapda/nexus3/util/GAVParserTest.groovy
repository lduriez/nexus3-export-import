package com.javapda.nexus3.util
import com.javapda.nexus3.nei.domain.*
import groovy.json.JsonOutput
/**
https://e.printstacktrace.blog/groovy-regular-expressions-the-definitive-guide/
*/
class GAVParserTest extends GroovyTestCase {

    void testParseGav() {
        checkValidGav("com.gs:gs-util:8.0.9-SNAPSHOT:test:jar")
        checkValidGav("com.gs:gs-util:8.0.9-SNAPSHOT:test:")
        checkValidGav("com.gs:gs-util:8.0.9-SNAPSHOT::")
        checkValidGav("com.gs:gs-util:8.0.9.RELEASE::jar")

    }

    void checkValidGav(String gavText) {
        def parsed = new GAVParser().parse(gavText)
        assert parsed
        if (false) {
            println("parsed: ${parsed}")
        }

    }

}