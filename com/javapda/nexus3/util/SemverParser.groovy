package com.javapda.nexus3.util
import java.text.SimpleDateFormat
import com.javapda.nexus3.nei.domain.*


/**
*/
class SemverParser {
    // 8.0.9-SNAPSHOT, 8.0.10.RELEASE
    static def PATTERN_SEMVER = /^([0-9]+)\.([0-9]+)\.([0-9]+)(-SNAPSHOT|\.RELEASE|\.RC[0-9]+)?$/
    def parse(String semverText) {
        def matches
        Integer major, minor, patch
        String suffix
        if ((matches = semverText =~ PATTERN_SEMVER)) {
            showGavAndMatches(semverText,matches)
            major=Integer.parseInt(matches.group(1))
            minor=Integer.parseInt(matches.group(2))
            patch=Integer.parseInt(matches.group(3))
            suffix=matches.group(4)
        } else {
            return null
        }
        def semver = new Semver(
                    semverText:semverText,
                    major:major,
                    minor:minor,
                    patch:patch,
                    suffix:suffix
                    )
        semver
    }

    def showGavAndMatches(semverText,matches) {
        if (false) {
            println("matches.groupCount(): ${matches.groupCount()}")
            println("pattern: ${PATTERN_SEMVER}")
            println("semverText ${semverText}")
            for (int i=0; i<= matches.groupCount();i++) {
                println("${i} ${matches.group(i)}")
            }
        }

    }

}