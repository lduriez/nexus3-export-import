package com.javapda.nexus3.util

class WebSiteLiveVerifier {
    URL url
    WebSiteLiveVerifier(String urlText) {
        if (!urlText) {
            throw new RuntimeException("null urlText given in ${this.class}")
        }
        url = new URL(urlText)
    }

    /**
    * if you give it an unknown host it will throw exception
    */
    boolean verify() {
        // final URL url = new URL(urlText);
        HttpURLConnection huc = (HttpURLConnection) url.openConnection();
        huc.setRequestMethod("HEAD");
        int responseCode = huc.getResponseCode();

        if (responseCode != 404) {
            return true
        } 
        return false

    }
}