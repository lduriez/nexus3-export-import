package com.javapda.nexus3.util
import java.text.SimpleDateFormat
import com.javapda.nexus3.nei.domain.*


/**
*/
class SemverParserTest extends GroovyTestCase {
    void test() {
        check('8.0.9-SNAPSHOT')
        check('8.0.10-SNAPSHOT')
        check('8.0.10.RELEASE')
        check('0.0.1')
        check('6.9.32.RC1123')
    }

    void check(String semverText) {
        Semver parsed = new SemverParser().parse(semverText)
        assert parsed
        if (false) {
            println("parsed: ${parsed}")
        }

    }

}