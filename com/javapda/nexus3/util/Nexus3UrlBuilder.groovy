package com.javapda.nexus3.util
import groovy.transform.ToString

@ToString(includeNames=true, includeFields=true, includePackage=false)
class Nexus3UrlBuilder<T extends Nexus3UrlBuilder> {
    String nexus3Url
    String path
    Map<String,Object> params = new HashMap<>()

    def addParam(Map<String,Object> datum) {
        params.putAll(datum)
        (T)this
    }

    def setNexus3Url(String nexus3Url) {
        this.nexus3Url = nexus3Url
        (T)this
    }
    def setPath(String path) {
        this.path = path
        (T)this
    }
    def build() {

        def res="${nexus3Url}${path?'/'+path:""}"
        if (params!=null && params.size()>0) {
            res+="?"
            res+=queryString()
        }
        res
    }
    def queryString() {
        params.collect { k,v -> "$k=$v" }.join('&')
    }
}