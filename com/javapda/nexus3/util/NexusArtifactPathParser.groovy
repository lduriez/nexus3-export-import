package com.javapda.nexus3.util
import java.text.SimpleDateFormat
import com.javapda.nexus3.nei.domain.*

/**
https://e.printstacktrace.blog/groovy-regular-expressions-the-definitive-guide/
*/
class NexusArtifactPathParser {
    static def PATTERN_MAVEN_METADATA_XML =  /^(\S+)\/maven-metadata\.xml\.(jar|tar\.gz|tgz|zip|pom|ear|war|md5|sha1)$/
    static def PATTERN_RELEASE = /^(\S+)\/(\S+)\/(\S+)\/((\2)-(\3)-?(\S*).(jar|tar\.gz|tgz|zip|pom|ear|war|md5|sha1))$/
    // static def PATTERN_SNAPSHOT = /^(\S+)\/(\S+)\/(\S+)-SNAPSHOT\/(\2)-(\3)-([:digit:]{8})-([:digit:]{6})-([:digit:]+)-?\.(\S*).(jar|pom|war|md5|sha1)$/    // ((\2)-(\3)-?(\S*).(jar|pom|war|md5|sha1))$/


    //  com/gs/gs-util/8.0.9-SNAPSHOT/gs-util-8.0.9-20200312.010211-24-sources.jar
    //  (com/gs)/(gs-util)/(8.0.9)-SNAPSHOT/gs-util-8.0.9-20200312.010211-24-sources.jar
    //  com/gs/gs-util/8.0.9-SNAPSHOT/gs-util-8.0.9-20200312.010211-24-applicationexception.jar.md5
    // static def PATTERN_SNAPSHOT = /^(\S+)\/(\S+)\/((\S+)-SNAPSHOT)\/(((\2)-(\4)-([0-9]{8})\.([0-9]{6}))-([0-9]+)(-(\S*))?\./ // ((\S+\.)|(\S+\.)((jar)\.))?(jar|pom|war|md5|sha1))$/   
    static def PATTERN_SNAPSHOT = /^(\S+)\/(\S+)\/((\S+)-SNAPSHOT)\/(((\2)-(\4)-([0-9]{8})\.([0-9]{6}))-([0-9]+)(-((\S+)\.)?((\S+)))?\.(jar|tar\.gz|tgz|zip|pom|ear|war|md5|sha1)?\.?(jar|tar.gz|tgz|zip|pom|ear|war|md5|sha1))$/   
    // static def PATTERN_SNAPSHOT = /^(\S+)\/(\S+)\/((\S+)-SNAPSHOT)\/(((\2)-(\4)-([0-9]{8})\.([0-9]{6}))-([0-9]+)(-((\S+)\.)?((\S+)\.))?(jar|pom|war|md5|sha1))$/   
    // static def PATTERN_SNAPSHOT = /^(\S+)\/(\S+)\/((\S+)-SNAPSHOT)\/(((\2)-(\4)-([0-9]{8})\.([0-9]{6}))-([0-9]+)(-(\S*))?\.(\S+\.)?(jar|pom|war|md5|sha1))$/   
    def parse(String path) {
        def matches // = path =~ PATTERN_RELEASE
        // def matches = path =~ /^(\S+)\/(\S+)\/(\S+)\/((\2)-(\3)-?(\S*).(jar|pom|war|md5|sha1))$/
        def groupId
        def artifactId
        def versionId
        def filename
        def classifier
        def packaging
        def underlyingPackaging
        def dateyyyyMMdd
        def timeHHmm
        def snapshotVersionId
        Integer snapshotCounter
        if ((matches = path =~ PATTERN_RELEASE)) {
            showPathAndMatches(path,matches)
            groupId=matches.group(1).replaceAll("/",".")
            artifactId=matches.group(2)
            versionId=matches.group(3)
            filename=matches.group(4)
            classifier=matches.group(7)
            packaging=matches.group(8)
            //throw new RuntimeException("invvalid non-compatible maven artifact path")
        } else if ((matches = path =~ PATTERN_SNAPSHOT)) {
            showPathAndMatches(path,matches)
            groupId=matches.group(1).replaceAll("/",".")
            artifactId=matches.group(2)
            snapshotVersionId=matches.group(3)
            versionId=matches.group(4)
            filename=matches.group(5)
            dateyyyyMMdd=matches.group(9)
            timeHHmm=matches.group(10)
            snapshotCounter=Integer.parseInt(matches.group(11))
            classifier=matches.group(14)
            underlyingPackaging=matches.group(17)
            packaging=matches.group(18)
            // throw new RuntimeException("TODO:SNAPSHOT:invalid non-compatible maven artifact path '${path}'")
        } else if ((matches = path =~ PATTERN_MAVEN_METADATA_XML)) {
            showPathAndMatches(path,matches)
            throw new RuntimeException("AGGY TODO:XML:invalid non-compatible maven artifact path '${path}'")
        } else {
            throw new RuntimeException("invalid non-compatible maven artifact path '${path}'")
        }
        return new NexusArtifactPathInfo(
                    path:path,
                    groupId:groupId,
                    artifactId:artifactId,
                    versionId:versionId,
                    classifier:classifier,
                    packaging:packaging,
                    underlyingPackaging:underlyingPackaging,
                    filename:filename,
                    dateyyyyMMdd:dateyyyyMMdd,
                    timeHHmm:timeHHmm,
                    snapshotVersionId:snapshotVersionId,
                    snapshotCounter:snapshotCounter
                    )
    }

    def showPathAndMatches(path,matches) {
        if (false) {
            println("matches.groupCount(): ${matches.groupCount()}")
            println("pattern: ${PATTERN_SNAPSHOT}")
            println("path ${path}")
            for (int i=0; i<= matches.groupCount();i++) {
                println("${i} ${matches.group(i)}")
            }
        }

    }


    static main(args) {
        def path="com/gs/gs-domain-services/1.0.0/gs-domain-services-1.0.0-sources.md5"
        def pathX="(com/gs)/(gs-domain-services)/(1.0.0)/((gs-domain-services)-(1.0.0)-(sources).(jar))"
        // println(new NexusArtifactPathParser().parse(path))
        println(new NexusArtifactPathParser().parse("com/gs/gs-domain-services/1.0.0/gs-domain-services-1.0.0-test.jar"))
        println(new NexusArtifactPathParser().parse("com/gs/gs-domain-services/1.0.0/gs-domain-services-1.0.0.jar"))
        // println(new NexusArtifactPathParser().parse("com/gs/gs-domain-services/1.0.0/gs-domain-services-1.0.0-pre-process.pom"))
    }

    static replacingMatching() {
        def version = "v3.4.23"

        def pattern = ~/^v(\d{1,3})\.(\d{1,3})\.\d{1,4}$/
        def matches = version =~ pattern
        println(matches[0..-1])
        println(matches.size())
        def newVersion = version.replaceFirst(pattern) { _,major,minor -> "v${major}.${(minor as int) + 1}.0"}

        assert newVersion == "v3.5.0"
        println("newVersion: ${newVersion}")
    }
}