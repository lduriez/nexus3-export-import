package com.javapda.nexus3.util
import com.javapda.nexus3.nei.domain.*
import groovy.json.JsonOutput
/**
https://e.printstacktrace.blog/groovy-regular-expressions-the-definitive-guide/
*/
class NexusArtifactPathParserTest extends GroovyTestCase {

    void testParseRelease() {
        def path="com/gs/gs-domain-services/1.0.0/gs-domain-services-1.0.0-sources.md5"
        checkValidReleasePath("com/gs/gs-domain-services/1.0.0/gs-domain-services-1.0.0-test.jar")
        checkValidReleasePath("com/gs/gs-domain-services/1.0.0/gs-domain-services-1.0.0-test.jar")
        checkValidReleasePath("com/gs/gs-domain-services/1.0.0/gs-domain-services-1.0.0.jar")

    }

    void testParseSnapshot() {
        checkValidSnapshotPath("com/gs/gs-util/8.0.9-SNAPSHOT/gs-util-8.0.9-20200312.010211-24-sources.jar")
        checkValidSnapshotPath("com/gs/gs-util/8.0.9-SNAPSHOT/gs-util-8.0.9-20200312.010211-25.jar")
        checkValidSnapshotPath("com/gs/gs-util/8.0.9-SNAPSHOT/gs-util-8.0.9-20200312.010211-24-applicationexception.jar.md5")
        checkValidSnapshotPath("com/gs/gs-util/8.0.9-SNAPSHOT/gs-util-8.0.9-20200312.010211-25.jar.md5")
        println(JsonOutput.toJson(new NexusArtifactPathParser().parse("com/gs/gs-util/8.0.9-SNAPSHOT/gs-util-8.0.9-20200312.010211-25.jar.md5")))
    }

    void testParseOther() {
        String path="bigfaceless/bfopdf-qrcode/maven-metadata.xml.sha1"
        def parsed = new NexusArtifactPathParser().parse(path)
        assert parsed
    }

    void checkValidSnapshotPath(String path) {
        def parsed = new NexusArtifactPathParser().parse(path)
        assert parsed
        if (false) {
            println("parsed: ${parsed}, \nrelease: ${parsed.getRelease()}, \nsnapshot: ${parsed.getSnapshot()}")
        }

    }

    void checkValidReleasePath(String path) {
        def parsed = new NexusArtifactPathParser().parse(path)
        assert parsed
        if (false) {
            println(parsed)
        }
    }

    static replacingMatching() {
        def version = "v3.4.23"

        def pattern = ~/^v(\d{1,3})\.(\d{1,3})\.\d{1,4}$/
        def matches = version =~ pattern
        println(matches[0..-1])
        println(matches.size())
        def newVersion = version.replaceFirst(pattern) { _,major,minor -> "v${major}.${(minor as int) + 1}.0"}

        assert newVersion == "v3.5.0"
        println("newVersion: ${newVersion}")
    }
}