package com.javapda.nexus3.util

class WebSiteLiveVerifierTest extends GroovyTestCase {
    void testValidUrls() {
        assert (new WebSiteLiveVerifier("https://cnn.com/").verify())
    }
    void testInvalidUrls() {
        assert !(new WebSiteLiveVerifier("https://cnn.com/BOGUS").verify())
    }
}