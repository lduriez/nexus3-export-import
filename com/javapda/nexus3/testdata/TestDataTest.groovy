package com.javapda.nexus3.testdata
import com.javapda.nexus3.nei.domain.*
import com.javapda.nexus3.nei.action.*

class TestDataTest extends GroovyTestCase {
    static boolean verbose
    void test() {
        TestData td = new TestData()
        checkNotNull td.localCredentialsFake()
        checkNotNull td.localNexus3ConfigReleases()
        checkNotNull td.nexusNexus3ConfigReleases()
        checkNotNull td.nexusNexus3ConfigThirdparty()
        checkNotNull td.nexus3SearchAssetsConfigThirdpartyBfopdf()
    }
    def checkNotNull(candidate) {
        assert candidate
        if (verbose) {
            println(candidate)
        }

    }

    


}