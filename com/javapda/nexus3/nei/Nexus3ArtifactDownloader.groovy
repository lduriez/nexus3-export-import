package com.javapda.nexus3.nei
import static java.lang.String.format
import groovy.json.JsonSlurper
import groovy.transform.ToString
import com.javapda.nexus3.nei.cli.Nexus3ArtifactDownloaderCLI
import com.javapda.nexus3.nei.domain.*
import com.javapda.nexus3.nei.action.*
import com.javapda.nexus3.nei.domain.config.*
import com.javapda.nexus3.util.*
import com.javapda.nexus3.nei.exceptions.*
import com.javapda.nexus3.rest.*

@ToString
class Nexus3ArtifactDownloader {
    Nexus3ArtifactDownloaderConfig config
    Nexus3ArtifactDownloader(Nexus3ArtifactDownloaderConfig config) {
        this.config = config
        verify()
    }

    def verify() {
        def reasons = []
        if (!new WebSiteLiveVerifier(config.repoUrl).verify()) {
            reasons << "bad url ${config.repoUrl} - not live"
        }
        if (!config.gav) {
            reasons << "missing gav"
        } else if (!config.gav.verify()) {
            reasons << "bad gav : not-verified"
        }
        if (!reasons.empty) {
            throw new RuntimeException("unable to download: ${reasons}")
        }
    }

    def download() {
        if (new WebSiteLiveVerifier(config.repoUrl).verify()) {
            // search for asset, if none throw exception, else pull first one
            Nexus3RepositoryAssets assets = new Nexus3SearchAssets().search(config.repoUrl,config.gav,config.credentials)
            if (assets.items.empty) {
                throw new Nexus3ArtifactNotFoundException("no artifact found with ${config.gav}")
            }
            Nexus3RepositoryAsset assetToDownload = assets.items[0]
            assetToDownload
            String destinationDirectory="."
            String filename = config.gav.filename
            File downloadedFile = new BinaryGet().get(assetToDownload.downloadUrl, new File(destinationDirectory,filename), config.credentials)
        } else {
            throw new RuntimeException("bad repoUrl: '${config.repoUrl}'")
        }
    }

    static main(args) {
            Nexus3ArtifactDownloaderCLI cli = Nexus3ArtifactDownloaderCLI.INSTANCE
            def allArgs = []
            // allArgs.addAll(prefs)
            allArgs.addAll(args)
            
            Nexus3ArtifactDownloaderConfig config = cli.parse(allArgs)
            if (config.verbose) {
                println("args: ${args}")
                println("config: ${config}")
            }
            new Nexus3ArtifactDownloader(config).download()
    }    

}
