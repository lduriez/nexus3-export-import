package com.javapda.nexus3.nei.action
import com.javapda.nexus3.nei.domain.*
import com.javapda.nexus3.rest.*
import com.javapda.nexus3.urlproviders.*

class Nexus3SearchAssets {
    // curl -X GET "https://some.domain.com/.../service/rest/v1/search/assets?maven.groupId=com.gs&maven.artifactId=gs-util&maven.baseVersion=8.0.9.RELEASE&maven.extension=jar" -H "accept: application/json"
    def search(Nexus3Config nexus3Config, Nexus3SearchAssetsConfig nexus3SearchAssetsConfig) {
        // build url with maven search info
        def url = getSearchUrl(nexus3Config.nexus3Url,nexus3SearchAssetsConfig)
        String json = getJson(url, nexus3Config.credentials)
        Nexus3RepositoryAssets.fromJson(json)
        
    }
    Nexus3RepositoryAssets search(String repoUrl, GAV gav, Credentials credentials, String repoName) {
        MavenAssetConfig mavenAssetConfig = new MavenAssetConfig(groupId:gav.groupId,artifactId:gav.artifactId,baseVersion:gav.version,classifier:gav.classifier,extension:gav.packaging)
        Nexus3SearchAssetsConfig nexus3SearchAssetsConfig = new Nexus3SearchAssetsConfig(mavenAssetConfig:mavenAssetConfig,repoName:repoName)
        def url = getSearchUrl(repoUrl,nexus3SearchAssetsConfig)
        println("url: ${url} in ${this.class.simpleName}")
        String json = getJson(url, credentials)
        Nexus3RepositoryAssets.fromJson(json)
    }
    private String getJson(String url, Credentials credentials) {
        new JsonGet(url:url, credentials:credentials).get()
    }
    private String getSearchUrl(String url, Nexus3SearchAssetsConfig nexus3SearchAssetsConfig) {
            new SearchAssetsUrl(url).get(nexus3SearchAssetsConfig)
    }
    Nexus3SearchAssets verify() {
        def reasons = []

        if (!reasons.empty) {
            throw new RuntimeException("bad ${this.class.simpleName} - ${reasons}")
        }
        this
    }



}