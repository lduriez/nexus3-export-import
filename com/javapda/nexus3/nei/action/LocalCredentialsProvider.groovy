package com.javapda.nexus3.nei.action
import com.javapda.nexus3.nei.domain.*
import groovy.json.JsonSlurper
import groovy.transform.ToString


@ToString(includeNames=true, includeFields=true)
class LocalCredentialsProvider {

    Credentials get(String installationName) {
        Nexus3Installation installation = Nexus3ExportImportPreferences.loadLocal().getInstallation(installationName)
        return installation?installation.credentials:null
    }

}