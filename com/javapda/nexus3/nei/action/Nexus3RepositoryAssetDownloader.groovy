package com.javapda.nexus3.nei.action
import com.javapda.nexus3.nei.domain.*
import com.javapda.nexus3.rest.*
class Nexus3RepositoryAssetDownloader {

    def download(Nexus3Config config, Nexus3RepositoryAsset asset, String destinationDirectory = System.properties['user.home']) {
        assert config
        assert asset
        assert destinationDirectory
        // println(config)
        // println(asset)
        // println(destinationDirectory)
        new BinaryGet().get(asset.downloadUrl, new File(destinationDirectory,asset.pathInfo.filename), config.credentials)
    }
}