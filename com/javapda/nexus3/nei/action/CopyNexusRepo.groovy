package com.javapda.nexus3.nei.action
import com.javapda.nexus3.nei.domain.*
import com.javapda.nexus3.nei.domain.config.*
import com.javapda.nexus3.nei.*
import groovy.json.JsonSlurper
import groovy.json.JsonParserType
import groovy.transform.ToString
@ToString(includeNames=true, includeFields=true)
class CopyNexusRepo {
    CopyNexusRepoConfig config
    def isNexus3UrlValid(repoConfig) {
        try {
            myprintln("repoConfig: ${repoConfig}")
            URL url = new URL(repoConfig.nexus3Url)
            myprintln("url: ${url}")
            def resp = url.getText()
            return true
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }
    def myprintln(msg) {
        if (config.verbose) {
            println("${msg}")
        }
    }
    def nexus3UrlRepositoriesList(nexus3Config) {
        // curl -X GET "http://localhost:8089/service/rest/beta/repositories" -H "accept: application/json"
        myprintln(nexus3Config)
        return "${nexus3Config.nexus3Url}/service/rest/v1/repositories"
    }
    def nexus3UrlRepositoryAssets(nexus3Config, nexus3Repo, continuationToken=null) {
        // curl -X GET "http://localhost:8089/service/rest/beta/repositories" -H "accept: application/json"
        def url= "${nexus3Config.nexus3Url}/service/rest/v1/assets?repository=${nexus3Repo.name}"
        if(continuationToken){
            url+="&continuationToken=${continuationToken}"
        }
        return url
    }
    def jsonGet(nexus3Config, url) {
        def connection = new URL( url )
                .openConnection() as HttpURLConnection
        
        if (!nexus3Config.credentials.empty) {
            String userpass = nexus3Config.credentials.colonSeparatedText;
            String basicAuth = "Basic " + new String(Base64.encoder.encode(userpass.getBytes()));
            connection.setRequestProperty ("Authorization", basicAuth);
        } else {
            myprintln("no credentials")
        }

        // set some headers
        connection.setRequestProperty( 'User-Agent', 'nexus-repo-3-groovy-2.4.4' )
        connection.setRequestProperty( 'Accept', 'application/json' )
        // get the response code - automatically sends the request
        return connection.inputStream.text
    }
    def repositories(nexus3Config) {
        def json = jsonGet(nexus3Config,nexus3UrlRepositoriesList(nexus3Config))
        // println(json)
        List<Nexus3Repo> rp = new JsonSlurper().parseText(json) as List<Nexus3Repo>
        myprintln("No. repositories: ${rp.size()}")
        return rp
    }
    List<Nexus3RepositoryAsset> repositoryAssets(nexus3Config, nexus3Repo) {
        myprintln("repositoryAssets")
        
        List<Nexus3RepositoryAsset> repositoryAssets = new ArrayList<>()
        def json = jsonGet(nexus3Config,nexus3UrlRepositoryAssets(nexus3Config,nexus3Repo,null))
        //println("json======${json}")
        
        Nexus3RepositoryAssets na = Nexus3RepositoryAssets.fromJson(json)
        
        def filter = { item -> 
            myprintln("item=====>${item} in ${this.class.simpleName}")
            String path=item.path
            myprintln("path=====>${path} in ${this.class.simpleName}")
            String extension = path.lastIndexOf(".") >-1 ? path.substring(path.lastIndexOf(".")+1):null
            myprintln("extension=====>${extension}")
            if (!extension) return false
            if (config.artifactPackagingFilters ) {
                myprintln("config.artifactPackagingFilters: ${config.artifactPackagingFilters}")
                //println("extension=====${extension}")
                if (!(extension in config.artifactPackagingFilters)) {
                    // println("extension '${extension}' is not in config.artifactPackagingFilters '${config.artifactPackagingFilters}'")
                    return false
                } else {
                    // println("extension '${extension}' is in config.artifactPackagingFilters '${config.artifactPackagingFilters}'")
                }
            }
            item.isValidPath() && ((nexus3Repo.name!="releases"&&nexus3Repo.name!="snapshots") || (item.semver && item.semver.major>=8))
        }
        na.items.each {
            it ->
            myprintln("--------------------------------------")
            myprintln("${it}")
        }
        repositoryAssets.addAll(na.items.grep(filter))
        boolean continuationTokenEnabled = true
        if (continuationTokenEnabled) {
            while (na.continuationToken!=null) {
                
                json = jsonGet(nexus3Config,nexus3UrlRepositoryAssets(nexus3Config, nexus3Repo,na.continuationToken))
                na = Nexus3RepositoryAssets.fromJson(json)
                repositoryAssets.addAll(na.items.grep(filter))
                myprintln("repositoryAssets.size(): ${repositoryAssets.size()}")
            }
        }
         myprintln("${repositoryAssets.size()} repositoryAssets for ${nexus3Repo}")
         return repositoryAssets
    }
    def reportErrors(reasons) {
        if (!reasons.empty) {
            throw new RuntimeException("problems: ${reasons}")
        }

    }
    def getRepository(Nexus3Config nc) {
        def match = repositories(nc).find { repo->
            myprintln("repo.name: ${repo.name} vs. ${nc.repoName}")
            return repo.name == nc.repoName
        }
        myprintln("match: ${match}")
        return match?new Nexus3Repo(name:match.name,format:match.format,type:match.type,url:match.url):null

    }
    def perform() {
        myprintln("verify repos exist in both areas")
        def reasons=[]
        if (!isNexus3UrlValid(config.source)) {
            reasons+="invalid repo url: ${config.source.nexus3Url}"
        } else if (!isNexus3UrlValid(config.target)) {
            reasons+="invalid repo url: ${config.target.nexus3Url}"
        }
        reportErrors(reasons)
        def sourceRepository = getRepository(config.source)
        if (!sourceRepository) {
            reasons+="no source repository named ${config.source.repoName}"
            reportErrors(reasons)
        }
        def targetRepository = getRepository(config.target)
        if (!targetRepository) {
            reasons+="no target repository named ${config.target.repoName}"
            reportErrors(reasons)
        }
        myprintln("config.target: ${config.target}")
        myprintln("sourceRepository: ${sourceRepository}")
        myprintln("config.source: ${config.source}")
        List<Nexus3RepositoryAsset> ras = repositoryAssets(config.source,sourceRepository)
        ras.eachWithIndex { nexus3RepositoryAsset, index ->
            
            transferRepositoryAsset(nexus3RepositoryAsset, index, ras.size())
        }
        println("ALL DONE in ${this.class.simpleName}")
    }
    def transferRepositoryAsset(Nexus3RepositoryAsset nexus3RepositoryAsset, int index=-1, int total=-2) {

        myprintln("${index+1}/${total} - ${nexus3RepositoryAsset}")
myprintln("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")        
        if (!assetExistsInTargetRepository(nexus3RepositoryAsset)) {
            myprintln("   downloading ${nexus3RepositoryAsset.downloadUrl} to ${config.tempDir}")
            File downloadedFile = download(nexus3RepositoryAsset)
            upload(nexus3RepositoryAsset,downloadedFile)
            myprintln("   ${nexus3RepositoryAsset.gav}")
        } else {
            myprintln("   asset already exists in target repository")
        }
    }
    def upload(Nexus3RepositoryAsset asset,File downloadedFile) {
        Nexus3ArtifactUploaderConfig nexus3ArtifactUploaderConfig = 
            new Nexus3ArtifactUploaderConfig(
             repoUrl:config.target.nexus3Url,
             repoName:config.target.repoName, 
             credentials:config.target.credentials,
             gav:asset.gav,
             filename:downloadedFile.absolutePath)
        myprintln("uploading : nexus3ArtifactUploaderConfig=${nexus3ArtifactUploaderConfig}")
        new Nexus3ArtifactUploader(nexus3ArtifactUploaderConfig).upload()

    }
    File download(Nexus3RepositoryAsset asset) {
        String destinationDirectory = config.tempDir
        File destDir=new File(config.tempDir)
        // myprintln("destDir: ${destDir.absolutePath}, exists: ${destDir.exists()}")
        if (!destDir.exists()) {
            if(destDir.mkdirs()) {
                myprintln("created directory '${destDir.absolutePath}'")
            }
        }
        File downloadedFile = new Nexus3RepositoryAssetDownloader().download(config.source, asset, destinationDirectory)
        if (downloadedFile) {
            myprintln("downloaded file: '${downloadedFile.absolutePath}'")
        } else {
            throw new RuntimeException("unable to download asset to '${destDir.absolutePath}'")
        }
        downloadedFile
    }
    boolean assetExistsInTargetRepository(Nexus3RepositoryAsset nexus3RepositoryAsset) {

        String repoUrl=config.target.nexus3Url
        String repoName=config.target.repoName
println("assetExistsInTargetRepository, repoUrl: ${repoUrl}, repoName=${repoName}")        
        GAV gav=nexus3RepositoryAsset.gav
        Credentials credentials=config.target.credentials
        myprintln("""
          *** checking asset existence ***
          repoName:     ${repoName}
          repoUrl:      ${repoUrl}
          gav:          ${gav}
          credentials:  ${credentials}
        """)
        Nexus3RepositoryAssets search = new Nexus3SearchAssets().search(repoUrl, gav, credentials, repoName)
        println("""
                search:             ${search}
                search.items.empty: ${search.items.empty}
                """)
        return !search.items.empty
    }
}