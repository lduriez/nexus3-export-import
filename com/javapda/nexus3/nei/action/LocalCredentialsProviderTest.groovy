package com.javapda.nexus3.nei.action
import com.javapda.nexus3.nei.domain.*
import groovy.json.JsonSlurper


class LocalCredentialsProviderTest extends GroovyTestCase {

    void testOne() {
        assert new LocalCredentialsProvider().get("localhost-fake")
        assert !new LocalCredentialsProvider().get("bogus")
    }

}