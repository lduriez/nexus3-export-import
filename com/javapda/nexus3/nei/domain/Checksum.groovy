package com.javapda.nexus3.nei.domain
import groovy.json.JsonSlurper
import groovy.transform.ToString

@ToString(ignoreNulls=false,includeNames=true, includeFields=true )
class Checksum {
    String sha1, md5

    static Checksum fromMap(Map map) {
        if (!map) return new Checksum()
        return new Checksum(
            sha1:map.containsKey('sha1')?map.sha1:null,
            md5:map.containsKey('md5')?map.md5:null
        )
    }
    static Checksum fromJson(String json) {
        Map<String, Object> map = new JsonSlurper().parseText(json)
        def submap=map.subMap(['sha1','md5'])
        new Checksum(submap)
    }

}