package com.javapda.nexus3.nei.domain
import com.javapda.nexus3.util.*
import groovy.transform.ToString
@ToString(includeNames=true, includeFields=true, includePackage=false)
class GAV {
    String gavText, groupId, artifactId, version, classifier, packaging

    static GAV create(String gavText) {
        GAV gav = new GAVParser().parse(gavText)
        if (!gav) {
            throw new RuntimeException("no GAV from GAVParser")
        }
        gav
    }

    GAV verify() {
        if (gavText) {
            def gav = new GAVParser().parse(gavText)
            groupId=gav.groupId
            artifactId=gav.artifactId
            version=gav.version
            classifier=gav.classifier
            packaging=gav.packaging
        } else {
            def reasons = []
            if (!groupId) reasons << "missing groupId"
            if (!artifactId) reasons << "missing artifactId"
            if (!version) reasons << "missing version"
            if (!packaging) reasons << "missing packaging"
            if (!reasons.empty) {
                throw new RuntimeException("bad gav data : ${reasons}")
            }
        }
        this
    }

    String getFilename() {
        return "${artifactId}${classifier?'-'+classifier:''}.${packaging}"
    }

}