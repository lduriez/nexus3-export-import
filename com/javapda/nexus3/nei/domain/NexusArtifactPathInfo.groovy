package com.javapda.nexus3.nei.domain
import groovy.json.JsonSlurper
import groovy.transform.ToString

@ToString(includeNames=true, includeFields=true, includePackage=false)
class NexusArtifactPathInfo {
    String path,groupId,versionId,artifactId,classifier,packaging,underlyingPackaging
    String filename
    String snapshotVersionId,dateyyyyMMdd,timeHHmm
    Integer snapshotCounter
    
    public Boolean getSnapshot() {
        snapshotVersionId!=null
    }
    public Boolean getRelease() {
        !getSnapshot()
    }
    public GAV getGav() {
        new GAV(
            groupId:groupId,
            version:versionId,
            artifactId:artifactId,
            classifier:classifier,
            packaging:packaging
        )
    }

    static NexusArtifactPathInfo fromJson(json) {
        Map<String, Object> map = new JsonSlurper().parseText(json)
        def submap=map.subMap(['path','groupId','versionId','artifactId','classifier','packaging','underlyingPackaging','filename',
        'snapshotVersionId','dateyyyyMMdd','timeHHmm','snapshotCounter'])
        new NexusArtifactPathInfo(submap)
    }

}