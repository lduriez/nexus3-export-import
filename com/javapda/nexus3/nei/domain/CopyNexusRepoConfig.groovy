package com.javapda.nexus3.nei.domain
import groovy.transform.ToString
@ToString(includeNames=true, includeFields=true)
class CopyNexusRepoConfig {
    Nexus3Config source, target
    String search, tempDir
    boolean saveArtifacts
    boolean verbose
    List<String> artifactPackagingFilters

    CopyNexusRepoConfig setVerbose(boolean verbose) {
        this.verbose=verbose
        if (this.verbose) {
            println("**********************************************************************")
            println("VERBOSE: ${this.toString()}")
            println("**********************************************************************")
        }
        this
    }

    
}