package com.javapda.nexus3.nei.domain
@Grapes([
    @Grab(group='org.yaml', module='snakeyaml', version='1.26'),
    @Grab(group='org.snakeyaml', module='snakeyaml-engine', version='1.0')
    ]
)

import org.yaml.snakeyaml.*
import org.yaml.snakeyaml.constructor.*
import org.snakeyaml.engine.v1.api.LoadSettings
import org.snakeyaml.engine.v1.api.LoadSettingsBuilder
import org.snakeyaml.engine.v1.api.Load
import groovy.json.JsonSlurper
import groovy.transform.ToString


@ToString(includeNames=true, includeFields=true)
class Nexus3ExportImportPreferences {
    static String DEFAULT_FILENAME=System.properties['user.home']+"/.nexus3-export-import.yaml"
    String description
    List<Nexus3Installation> installations
    List<String> artifactPackagingFilters
    boolean verbose
    String tempDir
    Nexus3Config source, target

    public List<String> getArtifactPackagingFilters() {
        artifactPackagingFilters
    }
    public void setArtifactPackagingFilters(List<String> artifactPackagingFilters) {
        this.artifactPackagingFilters = artifactPackagingFilters
    }

    public List<Nexus3Installation> getInstallations() {
        return installations
    }
    public void setInstallations(List<Nexus3Installation> installations) {
        this.installations = installations
    }

    public String getDescription() {
        return description
    }
    public void setDescription(String description) {
        this.description = description
    }
    public boolean isVerbose() {
        verbose
    }
    public void setVerbose(boolean verbose) {
        this.verbose=verbose
    }

    public Nexus3Installation getInstallation(String name) {
        for (Nexus3Installation inst : this.installations) {
            if (inst.name == name) {
                return inst
            }
        }
        return null
    }

    public static Nexus3ExportImportPreferences loadLocal(filename=DEFAULT_FILENAME) {
        File f = new File(filename)
        if (!f.exists()) {
            return null
        }
        def reader = f.newReader()
        TypeDescription t = new TypeDescription(Nexus3ExportImportPreferences)
        t.putListPropertyType("installations", Nexus3Installation)
        Constructor c =new Constructor(Nexus3ExportImportPreferences.class)
        c.addTypeDescription(t)
        Yaml yaml = new Yaml(c)
        (Nexus3ExportImportPreferences) yaml.load(reader)

    }

}

// @ToString(includeNames=true, includeFields=true)
// class Installation {
//     String name, nexus3Url
//     String credentials
//     boolean credentialsEncrypted

//     public String getName() {
//         return name
//     }
//     public void setName(String name) {
//         this.name = name
//     }

//     public String getCredentials() {
//         return credentials
//     }
//     public void setCredentials(String credentials) {
//         println("******************* set creds")
//         this.credentials = credentials
//     }
// }
