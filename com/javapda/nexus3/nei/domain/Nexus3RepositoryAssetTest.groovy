package com.javapda.nexus3.nei.domain
import groovy.json.JsonSlurper
import com.javapda.nexus3.util.*
import groovy.transform.ToString
@ToString(includeNames=true, includeFields=true)
class Nexus3RepositoryAssetTest extends GroovyTestCase {
    void testFromMap() {
        Map map =[download:'dl',path:'somepath',id:'someid',repository:'repo',format:'fmthere',checksum:[:]]
        Nexus3RepositoryAsset n = Nexus3RepositoryAsset.fromMap(map)
        assert n
        // println(n)
    }

    void testFromJson() {
        Object obj = new JsonSlurper().parseText(json)
        assert obj instanceof Map
        assert obj.downloadUrl 
        assert obj.path 
        assert obj.id 
        assert obj.repository 
        assert obj.format 
        assert obj.checksum 
        assert Nexus3RepositoryAsset.fromJson(json) instanceof Nexus3RepositoryAsset

    }


    static json = '''
        {
            "downloadUrl" : "http://localhost:8089/repository/thirdparty/bigfaceless/bfopdf-stamp/2.18.8/bfopdf-stamp-2.18.8.jar.sha1",
            "path" : "bigfaceless/bfopdf-stamp/2.18.8/bfopdf-stamp-2.18.8.jar.sha1",
            "id" : "dGhpcmRwYXJ0eTo3ZjYzNzlkMzJmOGRkNzhmNGE1ZjE0NjcwNWU0OTE0OA",
            "repository" : "thirdparty",
            "format" : "maven2",
            "checksum" : {
            "sha1" : "2561ae58469442f00018de28cb4b912442281427",
            "md5" : "d6ec9da10e1fb030295ad43b9e5bf010"
            }
        } 
    '''
}