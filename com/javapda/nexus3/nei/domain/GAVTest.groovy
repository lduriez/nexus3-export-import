package com.javapda.nexus3.nei.domain
import com.javapda.nexus3.util.*
import groovy.transform.ToString
@ToString(includeNames=true, includeFields=true)
class GAVTest extends GroovyTestCase {
    // String gavText, groupId, artifactId, version, classifier, packaging
    void test() {
        assert GAV.create("org.apache.commons:commons-lang3:3.10::jar")
        assert GAV.create("org.apache.commons:commons-lang3:3.10::jar").verify()
    }

}

/*
<!-- https://mvnrepository.com/artifact/org.apache.commons/commons-lang3 -->
<dependency>
    <groupId>org.apache.commons</groupId>
    <artifactId>commons-lang3</artifactId>
    <version>3.10</version>
</dependency>
*/