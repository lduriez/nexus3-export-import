package com.javapda.nexus3.nei.domain
import groovy.transform.ToString
@ToString(includeNames=true, includeFields=true)
class Nexus3RepoStorage {
    String blobStoreName,writePolicy
    Boolean strictContentTypeValidation
    
}