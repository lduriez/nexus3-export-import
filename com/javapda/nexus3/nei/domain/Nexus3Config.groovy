package com.javapda.nexus3.nei.domain
import groovy.transform.ToString

@ToString(includeNames=true, includeFields=true)
class Nexus3Config {
    String nexus3Url, repoName;
    Credentials credentials
    boolean verbose

    Nexus3Config verify() {
        def reasons = []
        if (!nexus3Url) reasons << "nexus3Url"
        if (!reasons.empty) {
            throw new RuntimeException("bad ${this.class.simpleName} - missing : ${reasons}")
        }
        this
    }

    static Nexus3Config create(Nexus3Installation installation, String repoName, boolean verbose=false) {
        new Nexus3Config(
            nexus3Url:installation.nexus3Url,
            credentials:installation.credentials,
            repoName:repoName,
            verbose:verbose
        )
    }


}