package com.javapda.nexus3.nei.domain
import groovy.transform.ToString
@ToString(includeNames=true, includeFields=true)
class Nexus3SearchAssetsConfig {
    MavenAssetConfig mavenAssetConfig
    String repoName

    String asQueryString() {
        def items=[]
        if (repoName) {
            items << "repository=${repoName}"
        }
        if (mavenAssetConfig) {
            items << mavenAssetConfig.asQueryString()
        }
        items.join('&')
    }
}
