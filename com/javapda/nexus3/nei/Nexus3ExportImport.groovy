package com.javapda.nexus3.nei
import static java.lang.String.format
import groovy.json.JsonSlurper
import groovy.transform.ToString
import com.javapda.nexus3.nei.cli.Nexus3ExportImportCLI
import com.javapda.nexus3.nei.domain.*
import com.javapda.nexus3.nei.action.*
@ToString
class Nexus3ExportImport {

    static main(args) {
            
            Nexus3ExportImportCLI cli = Nexus3ExportImportCLI.INSTANCE
            def allArgs = []
            allArgs.addAll(args)
            CopyNexusRepoConfig config = cli.parse(allArgs)
            if (config.verbose) {
                println("allArgs: ${allArgs}")
                println("config: ${config}")
                println("config.artifactPackagingFilters: ${config.artifactPackagingFilters}")
            }
            new CopyNexusRepo(config:config).perform()
    }    
}
