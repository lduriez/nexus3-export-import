package com.javapda.nexus3.nei.exceptions

class Nexus3ArtifactNotFoundException extends RuntimeException {

    Nexus3ArtifactNotFoundException(String message) {
        super(message)
    }
}