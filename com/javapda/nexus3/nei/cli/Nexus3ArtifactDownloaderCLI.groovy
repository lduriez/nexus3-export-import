package com.javapda.nexus3.nei.cli
import com.javapda.nexus3.nei.domain.*
import com.javapda.nexus3.util.*
import com.javapda.nexus3.nei.domain.config.*

enum Nexus3ArtifactDownloaderCLI {
    INSTANCE
    static final String PROG_NAME="nexus3DownloadArtifact.groovy"

    CliBuilder cliBuilder
    def footer() {
        """
         Examples:

         ./${PROG_NAME}  \\
            --nexus3Url http://localhost:8089 \\
            --repoCredentials "jed:wilma" 
            --repoName "releases" \\
            --gav com.javapda:my-artifact:0.0.1-SNAPSHOT:sources:jar \\
            --verbose false \\
            -------------------------------------------------
         ./${PROG_NAME}  \\
            --nexus3Url http://localhost:8089 \\
            --repoCredentials "trustedclient:trustedclient123" \\
            --gav org.apache.commons:commons-lang3:3.9::jar \\
            --verbose true
            -------------------------------------------------
        ./${PROG_NAME}  \\
            --nexus3Url http://localhost:8089 \\
            --repoCredentials "trustedclient:trustedclient123" \\
            --gav org.apache.commons:commons-lang3:3.9::jar \\
            --verbose true
            -------------------------------------------------
         ./${PROG_NAME} --help
            -------------------------------------------------

        """
    }

    Nexus3ArtifactDownloaderCLI() {
        cliBuilder = new CliBuilder(
                usage: "${PROG_NAME} [<options>]",
                header: 'Options:',
                footer: footer()
        )
        cliBuilder.width = 90  // default is 74
        cliBuilder.with {
            _(longOpt: 'nexus3Url',args:1,argName:'url',required:true,'main nexus url of repo [e.g. https://my.domain.com/nexus]')
            _(longOpt: 'repoName',args:1,argName:'name of nexus repositories',required:false,'name of repo [e.g. releases], if not provided will do full search')
            _(longOpt: 'repoCredentials',args:1,argName:'username:password',type: GString,required:false,'authentication credentials')
            _(longOpt: 'filename',args:1,argName:'path/to/file',type: GString,required:false,'name of file to upload, if not provided uses artifact and packaging to construct filename')
            _(longOpt: 'gav',args:1,argName:'gav to search',required:true,'gav - groupId:artifactId:version:classifier:packaging')
            h longOpt: 'help', 'Print this help text and exit.'
            v(longOpt: 'verbose',args:1,argName:'enabled', 'true or false, if true more output')
        }

    }

    Nexus3ArtifactDownloaderConfig parse(args) {
        OptionAccessor options = cliBuilder.parse(args)

        if (!options) {
            System.err << 'Error while parsing command-line options.\n'
            System.exit 1
        }

        if (options.h) {
            cliBuilder.usage()
            System.exit 0
        }
        def reasons = []
        def GAV gav
        def Credentials credentials
        def filename, repoName, nexus3Url
        if (options.gav) {
            gav = GAV.create(options.gav)
        } else {
            reasons << "gav"
        }
        if (options.filename) {
            filename = options.filename
        } else {
            filename = "${gav.artifactId}.${gav.packaging}"
        }
        if (options.nexus3Url) {nexus3Url=options.nexus3Url} else { reasons << "repoUrl"}
        if (options.repoCredentials) {
            credentials = new Credentials(options.repoCredentials)
        }

        Boolean verbose = Boolean.valueOf(options.verbose)
        new Nexus3ArtifactDownloaderConfig(
            gav:gav,
            repoName:repoName,
            repoUrl:nexus3Url,
            credentials:credentials,
            verbose:verbose).verify()
        
    }
}