#!/usr/bin/env groovy
import com.javapda.nexus3.nei.cli.*
import com.javapda.nexus3.nei.Nexus3ArtifactDownloader
import com.javapda.nexus3.nei.domain.*
import com.javapda.nexus3.util.time.*
import com.javapda.nexus3.util.*

/**
Note: if artifact not found throws: Nexus3ArtifactNotFoundException

*/

def startTime=new Date()
println("start:    ${startTime}")

Nexus3ArtifactDownloader.main(args)

def endTime=new Date()
println("end:      ${endTime}")
println("seconds:  ${Math.round((endTime.getTime()-startTime.getTime())/1000)} seconds")
println("duration: ${new DurationUtil().duration(startTime,endTime)}")
